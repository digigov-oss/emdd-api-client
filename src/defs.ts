export interface Api {
    getUnits: (params?: PaginationParams) => Promise<EmddUnit[]>;
    getOrganizations: (
        params?: PaginationParams,
    ) => Promise<EmddOrganization[]>;
    getRegistries: (params?: PaginationParams) => Promise<EmddRegistry[]>;
    getEvidences: (params?: PaginationParams) => Promise<EmddEvidence[]>;
    getProvisionOrgGroups: (
        params?: PaginationParams,
    ) => Promise<EmddProvisionOrgGroup[]>;
    getProcesses: (params?: GetProcessesOptions) => Promise<EmddProcessInfo[]>;
    getUnit: (id: Id) => Promise<EmddUnit>;
    getSdgCode: (id: Id) => Promise<EmddSdgCode>;
    getNaceCode: (id: Id) => Promise<EmddNaceCode>;
    getOrganization: (id: Id) => Promise<EmddOrganization>;
    getRegistry: (id: Id) => Promise<EmddRegistry>;
    getEvidence: (id: Id) => Promise<EmddEvidence>;
    getProvisionOrgGroup: (id: Id) => Promise<EmddProvisionOrgGroup>;

    getProcess: (
        id: string,
        opts?: GetProcessOptions,
    ) => Promise<EmddProcess | EmddExtendedProcess>;

    getExtendedProcess: (
        id: string,
        opts?: GetProcessOptions,
    ) => Promise<EmddExtendedProcess>;
}

export type ApiResponse<T> = Record<string, any> & {
    success: boolean;
    data: T;
    message?: string;
};

export interface PagedApiResponse<T> extends ApiResponse<T> {
    limit: number;
    current_page: number;
    next_page: number;
    total: number;
}

export interface EmddUnit {
    id: string;
    parent_id?: string;
    title: {
        el: string;
        en?: string;
    };
    active?: boolean;
    metadata?: {
        description?: string;
        email?: string;
        telephone?: string;
        fulladdress?: string;
        adminunitlevel1?: string;
        adminunitlevel2?: string;
        postcode?: string;
        supervisorunitcode?: string;
        organizationcode?: string;
    };
}

export interface EmddSdgCode {
    id: string;
    title: {
        el: string;
        en?: string;
    };
    description?: {
        el: string;
        en?: string;
    };
    searched_code?: string;
}

export interface EmddNaceCode {
    id: string;
    title: {
        el: string;
        en?: string;
    };
    full_code?: string;
    light_code?: string;
    searched_code?: string;
}

export interface EmddOrganization {
    id: string;
    parent_id?: string;
    descendants_count?: number;
    title: {
        el: string;
        en?: string;
    };
    active?: boolean;
    metadata?: {
        description?: string;
        'alternative-title'?: string;
        email?: string;
        telephone?: string;
        purpose?: string;
        fulladdress?: string;
        adminunitlevel1?: string;
        adminunitlevel2?: string;
        postcode?: string;
        vatid?: string;
        status?: string;
        url?: string;
        foundationdate?: string;
    };
}

export interface EmddRegistry {
    id: string;
    title: {
        el: string;
        en?: string;
    };
}

export interface EmddEvidence {
    id: number;
    title: {
        el: string;
        en?: string;
    };
    wiki_connection?: boolean;
    type_of?: string[];
    related?: {
        id: string;
        ns?: string;
        title?: {
            el: string;
            en?: string;
        };
    };
}

export interface EmddProvisionOrgGroup {
    id: string;
    title: {
        el: string;
        en?: string;
    };
    url?: string;
}

export interface ProcessCondition {
    conditions_num_id: number;
    conditions_alternative: boolean;
    conditions_alternative_of_selected?: string;
    conditions_alternative_of?: number;
    conditions_name: string;
    conditions_type?: string;
    conditions_url?: string;
}

export interface ProcessEvidence<ET> {
    evidence_num_id: number;
    evidence_is_under_prerequisite?: boolean;
    evidence_prerequisite?: string[];
    evidence_alternative: boolean;
    evidence_submission_type: string[];
    evidence_owner: string[];
    evidence_type: ET;
    evidence_description: string;
    evidence_alternative_of?: number;
    evidence_alternative_of_selector?: string;
    evidence_note?: string;
    evidence_related_process?: {
        id: string;
        title: {
            el: string;
            en?: string;
        };
        ns?: string;
        last_updated?: string;
    }[];
    evidence_related_url?: string;
}

export interface ProcessEvidenceCost {
    evidence_cost_num_id: number;
    evidence_cost_alternative: boolean;
    evidence_cost_alternative_of?: number;
    evidence_cost_alternative_of_selector?: string;
    evidence_cost_type: string;
    evidence_cost_code?: string;
    evidence_cost_calculation?: string[];
    evidence_cost_description: string;
    evidence_cost_min?: number;
    evidence_cost_max?: number;
    evidence_cost_payment_type?: string[];
    evidence_cost_url?: string;
}

export interface ProcessProvisionDigitalLocation {
    provision_digital_location_title: string;
    provision_digital_location_url: string;
}

export interface Process<NaceT, SdgT, OrgT, RegT, PogT, UnitT> {
    alternative_titles?: string[];
    application_description?: string;
    application_note?: string;
    application_owner?: string[];
    application_submission_type?: string[];
    application_type?: string;
    application_related_url?: string;
    border_provision?: string[];
    bpmn_digital_source?: string;
    bpmn_source?: string;
    cost_max?: number;
    cost_min?: number;
    deadline_duration?: string;
    description: string;
    estimated_implementation_duration?: string;
    evidence_alternative_total_number: number;
    evidence_cost_guarantee: number;
    evidence_cost_total_number: number;
    evidence_ex_officio_total_number: number;
    evidence_identification_type?: string[];
    evidence_prerequisite_total_number: number;
    evidence_step_digital_total_number: number;
    evidence_step_total_number: number;
    evidence_total_number: number;
    id: string;
    interval?: string;
    life_events: string[];
    nace_codes?: NaceT[];
    tags?: string[];
    official_title: string;
    org_owner: OrgT | undefined;
    org_owner_is_private: boolean;
    output_registries?: RegT[];
    output_type: string[];
    provided_language: string[];
    provided_to: string[];
    provision_org: OrgT[];
    provision_org_directory: string[];
    provision_org_group: PogT[];
    provision_org_group_remote: PogT[];
    provision_org_group_remote_urls: string;
    is_served_via_mykeplive?: boolean;
    provision_org_owner_directory: UnitT[];
    remarks: string;
    sdg_codes?: SdgT[];
    sdg_lack_of_response_rule?: string;
    sdg_notes?: string;
    sdg_resource?: string;
    sdg_resource_other?: string;
    see_also?: {
        id: string;
        title: {
            el: string;
            en?: string;
        };
        ns?: string;
        last_updated?: string;
    }[];
    source: string[];
    tax_type: string[];
    total_duration_steps_digital_max: string;
    total_duration_steps_digital_min: string;
    total_duration_steps_max?: string;
    total_duration_steps_min?: string;
    trigger: string[];
    trigger_type: string[];
    type: string;
    usage: string[];
    uuid: string;
    validity_duration: string;
}

export interface ProcessRule {
    rule_type?: string;
    rule_decision_number: string;
    rule_decision_year: string;
    rule_article?: string;
    rule_description: string;
    rule_gazette_doc_issue?: string;
    rule_gazette_doc_number?: number;
    rule_url?: string;
}

export interface ProcessStep {
    step_child: boolean;
    step_description?: string;
    step_duration_max?: number;
    step_duration_min?: number;
    step_duration_type?: string;
    step_exit: boolean;
    step_implementation?: string;
    step_note?: string;
    step_num_id: number;
    step_official?: string;
    step_previous_child?: string;
    step_previous_child_selector?: string;
    step_title: string;
}

export interface ProcessStepDigital {
    step_digital_num_id: number;
    step_digital_exit: boolean;
    step_digital_child: boolean;
    step_digital_title: string;
    step_digital_official?: string;
    step_digital_implementation?: string;
    step_digital_url?: string;
    step_digital_duration_min?: number;
    step_digital_duration_max?: number;
    step_digital_duration_type?: string;
    step_digital_description?: string;
    step_digital_note?: string;
    step_digital_previous_child?: string;
    step_digital_previous_child_selector?: string;
}

export interface ProcessUsefulLink {
    useful_link_title: string;
    useful_link_url: string;
}

export interface ProcessGlance {
    cost?: {
        title: string;
        value: string;
    };
    description?: {
        title: string;
        value: string;
    };
    digital_service_points?: {
        title: string;
        value: {
            title: string;
            description?: string;
            url?: string;
        }[];
    };
    duration_steps_digital?: {
        title: string;
        value: string;
    };
    estimated_time?: {
        title: string;
        value: string;
    };
    evidences?: {
        title: string;
        value: string;
    };
    remote_service_points?: {
        title: string;
        value: {
            title: string;
            url?: string;
        }[];
    };
    service_points?: {
        title: string;
        value: {
            title: string;
            url?: string;
        }[];
    };
}

export interface EmddProcessInfo {
    ns: string;
    title: {
        el: string;
        en?: string;
    };
    id: string;
    uuid: string;
    url: string;
    last_updated: string;
}

export interface EmddProcess extends EmddProcessInfo {
    metadata: {
        process: Process<string, string, string, string, string, string>;
        process_conditions: ProcessCondition[];
        process_evidences: ProcessEvidence<string>[];
        process_evidences_cost?: ProcessEvidenceCost[];
        process_provision_digital_locations?: ProcessProvisionDigitalLocation[];
        process_rules?: ProcessRule[];
        process_steps?: ProcessStep[];
        process_steps_digital?: ProcessStepDigital[];
        process_useful_links?: ProcessUsefulLink[];
        glance?: ProcessGlance;
    };
}

export interface EmddExtendedProcess extends EmddProcessInfo {
    metadata: {
        process: Process<
            EmddNaceCode,
            EmddSdgCode,
            EmddUnit,
            EmddRegistry,
            EmddProvisionOrgGroup,
            EmddUnit
        >;
        process_conditions: ProcessCondition[];
        process_evidences: ProcessEvidence<EmddEvidence>[];
        process_evidences_cost?: ProcessEvidenceCost[];
        process_provision_digital_locations?: ProcessProvisionDigitalLocation[];
        process_rules?: ProcessRule[];
        process_steps?: ProcessStep[];
        process_steps_digital?: ProcessStepDigital[];
        process_useful_links?: ProcessUsefulLink[];
        glance?: ProcessGlance;
    };
}

export type Id = string | number;
export interface PaginationParams {
    page?: number;
    limit?: number;
}

export interface GetProcessOptions {
    english?: true;
}

export interface GetProcessesOptions extends PaginationParams {
    filter?: string;
    english?: true;
}
