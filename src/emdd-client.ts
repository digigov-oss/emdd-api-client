import {
    Api,
    ApiResponse,
    EmddEvidence,
    EmddExtendedProcess,
    EmddNaceCode,
    EmddOrganization,
    EmddProcess,
    EmddProcessInfo,
    EmddProvisionOrgGroup,
    EmddRegistry,
    EmddSdgCode,
    EmddUnit,
    GetProcessesOptions,
    GetProcessOptions,
    Id,
    PagedApiResponse,
    PaginationParams,
} from '.';
import { executeOnCondition } from './utils';

export type Headers = Record<string, string>;
export interface HttpGetParams {
    headers?: Headers;
    params?: Record<string, any>;
}

export interface HttpReader {
    get: <T>(url: string, config?: HttpGetParams) => Promise<T>;
}

export interface HttpEmddClientConfig {
    apiUrl?: string;
    apiKey?: string;
    maxRequestsPerSecond?: number;
}

export class HttpEmddClient implements Api {
    private readonly reader: HttpReader;
    private readonly apiUrl: string;
    private readonly apiKey?: string;

    private readonly apiProcessesSearchUrl: string;
    private readonly apiProcessesUrl: string;
    private readonly apiExtendedProcessesUrl: string;

    private readonly apiOrganizationUrl: string;
    private readonly apiNaceCodeUrl: string;
    private readonly apiSdgCodeUrl: string;
    private readonly apiUnitUrl: string;
    private readonly apiRegistryUrl: string;
    private readonly apiEvidenceUrl: string;
    private readonly apiProvisionOrgGroupUrl: string;

    private readonly apiOrganizationsUrl: string;
    private readonly apiUnitsUrl: string;
    private readonly apiRegistriesUrl: string;
    private readonly apiEvidencesUrl: string;
    private readonly apiProvisionOrgGroupsUrl: string;

    private readonly defaultHeaders: Record<string, string>;
    private readonly maxRequestsPerSecond: number;

    private activeRequestDates = new Array<number>();

    constructor(reader: HttpReader, opts?: HttpEmddClientConfig) {
        this.apiUrl = opts?.apiUrl ?? 'https://api.digigov.grnet.gr';
        this.apiKey = opts?.apiKey;
        this.apiProcessesSearchUrl = `${this.apiUrl}/v1/services/search`;
        this.apiProcessesUrl = `${this.apiUrl}/v1/services`;
        this.apiExtendedProcessesUrl = `${this.apiUrl}/v1/services-extended`;

        this.apiOrganizationUrl = `${this.apiUrl}/v1/organization`;
        this.apiUnitUrl = `${this.apiUrl}/v1/unit`;
        this.apiNaceCodeUrl = `${this.apiUrl}/v1/registries/nace`;
        this.apiSdgCodeUrl = `${this.apiUrl}/v1/registries/sdg`;
        this.apiRegistryUrl = `${this.apiUrl}/v1/registry/id`;
        this.apiEvidenceUrl = `${this.apiUrl}/v1/registries/evidence`;
        this.apiProvisionOrgGroupUrl = `${this.apiUrl}/v1/registries/evidence/taxonomy/provision_org_gro`;

        this.apiOrganizationsUrl = `${this.apiUrl}/v1/organizations`;
        this.apiUnitsUrl = `${this.apiUrl}/v1/units`;
        this.apiRegistriesUrl = `${this.apiUrl}/v1/registry/all`;
        this.apiEvidencesUrl = `${this.apiUrl}/v1/registries/evidence/all`;
        this.apiProvisionOrgGroupsUrl = `${this.apiUrl}/v1/registries/evidence/taxonomy/provision_org_gro`;

        this.maxRequestsPerSecond = opts?.maxRequestsPerSecond ?? 10;

        this.defaultHeaders = {
            'Cache-Control': 'no-cache',
            'User-Agent':
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
        };
        if (this.apiKey) {
            this.defaultHeaders['x-api-key'] = `${this.apiKey}`;
        }
        this.reader = reader;
    }

    //No plural available
    getSdgCode = async (id: Id) => {
        return this.getOne<EmddSdgCode>({
            url: `${this.apiSdgCodeUrl}/${id}`,
        });
    };
    //No plural available
    getNaceCode = async (id: Id) => {
        return this.getOne<EmddNaceCode>({
            url: `${this.apiNaceCodeUrl}/${id}`,
        });
    };

    getUnit = async (id: Id) => {
        return this.getOne<EmddUnit>({ url: this.apiUnitUrl, params: { id } });
    };

    getOrganization = async (id: Id) => {
        return this.getOne<EmddOrganization>({
            url: this.apiOrganizationUrl,
            params: { id },
        });
    };

    getProvisionOrgGroup = async (id: Id) => {
        return this.getOne<EmddProvisionOrgGroup>({
            url: `${this.apiProvisionOrgGroupUrl}/${id}`,
        }).then((r) => {
            const res: EmddProvisionOrgGroup = {
                id: r.id,
                title: r.title,
                url: r.url,
            };
            return res;
        });
    };

    getRegistry = async (id: Id) => {
        return this.getOne<EmddRegistry>({
            url: `${this.apiRegistryUrl}/${id}`,
        });
    };

    getEvidence = async (id: Id) => {
        return this.getOne<EmddEvidence>({
            url: `${this.apiEvidenceUrl}/${id}`,
        });
    };

    getProcess = async (id: Id, opts?: GetProcessOptions) => {
        const params: Record<string, string> = {};
        if (opts?.english === true) {
            params['lang'] = 'en';
        }
        return this.getOne<EmddProcess>({
            url: `${this.apiProcessesUrl}/${id}`,
            params,
        });
    };

    getExtendedProcess = async (id: Id, opts?: GetProcessOptions) => {
        const params: Record<string, string> = {};
        if (opts?.english === true) {
            params['lang'] = 'en';
        }
        return this.getOne<EmddExtendedProcess>({
            url: `${this.apiExtendedProcessesUrl}/${id}`,
            params,
        });
    };

    getProcesses = async (opts?: GetProcessesOptions) => {
        let url = `${this.apiProcessesUrl}`;
        if (opts?.filter) {
            url = `${this.apiProcessesSearchUrl}/filter/${opts.filter}`;
        }
        const params: Record<string, any> = {};
        if (opts?.english === true) {
            params.lang = 'en';
        }
        if (opts?.limit || opts?.page) {
            params.page = opts.page;
            params.limit = opts.limit;
            return this.getPage<EmddProcessInfo>({
                url,
                params: params,
            }).then((r) => r.data);
        }
        return this.exhaustAPI<EmddProcessInfo>({ url, params });
    };

    getUnits = async (params?: PaginationParams) => {
        const url = this.apiUnitsUrl;
        if (params) {
            return this.getPage<EmddUnit>({ url, params }).then((r) => r.data);
        }
        return this.exhaustAPI<EmddUnit>({ url });
    };

    getOrganizations = async (params?: PaginationParams) => {
        const url = this.apiOrganizationsUrl;
        if (params) {
            return this.getPage<EmddOrganization>({ url, params }).then(
                (r) => r.data,
            );
        }
        return this.exhaustAPI<EmddOrganization>({ url });
    };

    getRegistries = async (params?: PaginationParams) => {
        const url = this.apiRegistriesUrl;
        if (params) {
            return this.getPage<EmddRegistry>({ url, params }).then(
                (r) => r.data,
            );
        }
        return this.exhaustAPI<EmddRegistry>({ url });
    };

    getEvidences = async () => {
        const url = this.apiEvidencesUrl;
        return this.get<PagedApiResponse<EmddEvidence[]>>({ url }).then(
            (r) => r.data,
        );
    };

    getProvisionOrgGroups = async (params?: PaginationParams) => {
        const url = this.apiProvisionOrgGroupsUrl;
        if (params) {
            return this.getPage<EmddProvisionOrgGroup>({ url, params }).then(
                (r) => r.data,
            );
        }
        return this.exhaustAPI<EmddProvisionOrgGroup>({ url });
    };

    private async getPage<T>({
        url,
        params,
    }: {
        url: string;
        params?: Record<string, any>;
    }) {
        return this.get<PagedApiResponse<T[]>>({
            url,
            params,
        }).then((r) => {
            if (!r.success) {
                //TODO fix this
                // throw new Error(r.message ?? 'Error')};
                return {
                    data: [],
                    current_page: params?.page,
                    next_page: params?.page,
                    limit: params?.limit,
                    total: 0,
                };
            }
            return r;
        });
    }

    private exhaustAPI = <T>({
        url,
        params,
    }: {
        url: string;
        params?: Record<string, any>;
    }): Promise<T[]> => {
        const limit = 100;
        return new Promise((resolve, reject) => {
            const allPages: Record<number, T[]> = [];
            const page = 1;
            return this.getPage<T>({
                url,
                params: Object.assign({}, params, { page, limit }),
            })
                .then((r) => {
                    const total = r.total;
                    let pages = Math.floor(total / limit);
                    if (total % limit !== 0) {
                        pages = pages + 1;
                    }
                    const promises = Array<Promise<void>>();
                    allPages[1] = r.data;
                    for (let page = 2; page <= pages; page++) {
                        promises.push(
                            this.getPage<T>({
                                url,
                                params: Object.assign({}, params, {
                                    page,
                                    limit,
                                }),
                            }).then((r) => {
                                allPages[page] = r.data;
                            }),
                        );
                    }
                    Promise.all(promises)
                        .then(() => {
                            let res = Array<T>();
                            const dataArray = Object.entries(allPages)
                                .sort((a, b) => {
                                    if (a[0] === b[0]) return 0;
                                    return a[0] < b[0] ? -1 : 1;
                                })
                                .map((a) => {
                                    return a[1];
                                });
                            res = res.concat(...dataArray);
                            resolve(res);
                        })
                        .catch(reject);
                })
                .catch(reject);
        });
    };

    private getOne = async <T>(opts: {
        url: string;
        params?: any;
    }): Promise<T> => {
        return this.get<ApiResponse<T>>(opts).then((r) => {
            if (!r.success) throw new Error(r.message ?? 'Error');
            return r.data;
        });
    };

    private get = async <T>(opts: {
        url: string;
        params?: Record<string, any>;
    }): Promise<T> => {
        const url = opts.url;
        return executeOnCondition<T>({
            f: async () => {
                return this.reader.get<T>(url, {
                    params: opts?.params,
                    headers: this.defaultHeaders,
                });
            },
            condition: () => {
                if (
                    this.activeRequestDates.length < this.maxRequestsPerSecond
                ) {
                    return true;
                }
                return new Date().getTime() - 1000 > this.activeRequestDates[0];
            },
            beforeExecute: () => {
                this.storeRequest();
            },
            delay: 50,
        });
    };

    private storeRequest = () => {
        this.activeRequestDates.push(new Date().getTime());
        const l = this.activeRequestDates.length;
        if (l > this.maxRequestsPerSecond) {
            this.activeRequestDates.shift();
        }
    };
}
