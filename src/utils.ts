export function executeOnCondition<T>(props: {
    f: () => Promise<T>;
    condition: () => boolean;
    beforeExecute?: () => void;
    delay: number;
}): Promise<T> {
    const { f, beforeExecute, condition } = props;
    return new Promise<T>((resolve, reject) => {
        function doExec() {
            beforeExecute && beforeExecute();
            f().then(resolve).catch(reject);
        }
        if (condition()) {
            doExec();
        } else {
            const interval = setInterval(() => {
                if (condition()) {
                    clearInterval(interval);
                    doExec();
                }
            }, props.delay);
        }
    });
}
