"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpEmddClient = void 0;
const utils_1 = require("./utils");
class HttpEmddClient {
    constructor(reader, opts) {
        var _a, _b;
        this.activeRequestDates = new Array();
        //No plural available
        this.getSdgCode = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.getOne({
                url: `${this.apiSdgCodeUrl}/${id}`,
            });
        });
        //No plural available
        this.getNaceCode = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.getOne({
                url: `${this.apiNaceCodeUrl}/${id}`,
            });
        });
        this.getUnit = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.getOne({ url: this.apiUnitUrl, params: { id } });
        });
        this.getOrganization = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.getOne({
                url: this.apiOrganizationUrl,
                params: { id },
            });
        });
        this.getProvisionOrgGroup = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.getOne({
                url: `${this.apiProvisionOrgGroupUrl}/${id}`,
            }).then((r) => {
                const res = {
                    id: r.id,
                    title: r.title,
                    url: r.url,
                };
                return res;
            });
        });
        this.getRegistry = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.getOne({
                url: `${this.apiRegistryUrl}/${id}`,
            });
        });
        this.getEvidence = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.getOne({
                url: `${this.apiEvidenceUrl}/${id}`,
            });
        });
        this.getProcess = (id, opts) => __awaiter(this, void 0, void 0, function* () {
            const params = {};
            if ((opts === null || opts === void 0 ? void 0 : opts.english) === true) {
                params['lang'] = 'en';
            }
            return this.getOne({
                url: `${this.apiProcessesUrl}/${id}`,
                params,
            });
        });
        this.getExtendedProcess = (id, opts) => __awaiter(this, void 0, void 0, function* () {
            const params = {};
            if ((opts === null || opts === void 0 ? void 0 : opts.english) === true) {
                params['lang'] = 'en';
            }
            return this.getOne({
                url: `${this.apiExtendedProcessesUrl}/${id}`,
                params,
            });
        });
        this.getProcesses = (opts) => __awaiter(this, void 0, void 0, function* () {
            let url = `${this.apiProcessesUrl}`;
            if (opts === null || opts === void 0 ? void 0 : opts.filter) {
                url = `${this.apiProcessesSearchUrl}/filter/${opts.filter}`;
            }
            const params = {};
            if ((opts === null || opts === void 0 ? void 0 : opts.english) === true) {
                params.lang = 'en';
            }
            if ((opts === null || opts === void 0 ? void 0 : opts.limit) || (opts === null || opts === void 0 ? void 0 : opts.page)) {
                params.page = opts.page;
                params.limit = opts.limit;
                return this.getPage({
                    url,
                    params: params,
                }).then((r) => r.data);
            }
            return this.exhaustAPI({ url, params });
        });
        this.getUnits = (params) => __awaiter(this, void 0, void 0, function* () {
            const url = this.apiUnitsUrl;
            if (params) {
                return this.getPage({ url, params }).then((r) => r.data);
            }
            return this.exhaustAPI({ url });
        });
        this.getOrganizations = (params) => __awaiter(this, void 0, void 0, function* () {
            const url = this.apiOrganizationsUrl;
            if (params) {
                return this.getPage({ url, params }).then((r) => r.data);
            }
            return this.exhaustAPI({ url });
        });
        this.getRegistries = (params) => __awaiter(this, void 0, void 0, function* () {
            const url = this.apiRegistriesUrl;
            if (params) {
                return this.getPage({ url, params }).then((r) => r.data);
            }
            return this.exhaustAPI({ url });
        });
        this.getEvidences = () => __awaiter(this, void 0, void 0, function* () {
            const url = this.apiEvidencesUrl;
            return this.get({ url }).then((r) => r.data);
        });
        this.getProvisionOrgGroups = (params) => __awaiter(this, void 0, void 0, function* () {
            const url = this.apiProvisionOrgGroupsUrl;
            if (params) {
                return this.getPage({ url, params }).then((r) => r.data);
            }
            return this.exhaustAPI({ url });
        });
        this.exhaustAPI = ({ url, params, }) => {
            const limit = 100;
            return new Promise((resolve, reject) => {
                const allPages = [];
                const page = 1;
                return this.getPage({
                    url,
                    params: Object.assign({}, params, { page, limit }),
                })
                    .then((r) => {
                    const total = r.total;
                    let pages = Math.floor(total / limit);
                    if (total % limit !== 0) {
                        pages = pages + 1;
                    }
                    const promises = Array();
                    allPages[1] = r.data;
                    for (let page = 2; page <= pages; page++) {
                        promises.push(this.getPage({
                            url,
                            params: Object.assign({}, params, {
                                page,
                                limit,
                            }),
                        }).then((r) => {
                            allPages[page] = r.data;
                        }));
                    }
                    Promise.all(promises)
                        .then(() => {
                        let res = Array();
                        const dataArray = Object.entries(allPages)
                            .sort((a, b) => {
                            if (a[0] === b[0])
                                return 0;
                            return a[0] < b[0] ? -1 : 1;
                        })
                            .map((a) => {
                            return a[1];
                        });
                        res = res.concat(...dataArray);
                        resolve(res);
                    })
                        .catch(reject);
                })
                    .catch(reject);
            });
        };
        this.getOne = (opts) => __awaiter(this, void 0, void 0, function* () {
            return this.get(opts).then((r) => {
                var _a;
                if (!r.success)
                    throw new Error((_a = r.message) !== null && _a !== void 0 ? _a : 'Error');
                return r.data;
            });
        });
        this.get = (opts) => __awaiter(this, void 0, void 0, function* () {
            const url = opts.url;
            return (0, utils_1.executeOnCondition)({
                f: () => __awaiter(this, void 0, void 0, function* () {
                    return this.reader.get(url, {
                        params: opts === null || opts === void 0 ? void 0 : opts.params,
                        headers: this.defaultHeaders,
                    });
                }),
                condition: () => {
                    if (this.activeRequestDates.length < this.maxRequestsPerSecond) {
                        return true;
                    }
                    return new Date().getTime() - 1000 > this.activeRequestDates[0];
                },
                beforeExecute: () => {
                    this.storeRequest();
                },
                delay: 50,
            });
        });
        this.storeRequest = () => {
            this.activeRequestDates.push(new Date().getTime());
            const l = this.activeRequestDates.length;
            if (l > this.maxRequestsPerSecond) {
                this.activeRequestDates.shift();
            }
        };
        this.apiUrl = (_a = opts === null || opts === void 0 ? void 0 : opts.apiUrl) !== null && _a !== void 0 ? _a : 'https://api.digigov.grnet.gr';
        this.apiKey = opts === null || opts === void 0 ? void 0 : opts.apiKey;
        this.apiProcessesSearchUrl = `${this.apiUrl}/v1/services/search`;
        this.apiProcessesUrl = `${this.apiUrl}/v1/services`;
        this.apiExtendedProcessesUrl = `${this.apiUrl}/v1/services-extended`;
        this.apiOrganizationUrl = `${this.apiUrl}/v1/organization`;
        this.apiUnitUrl = `${this.apiUrl}/v1/unit`;
        this.apiNaceCodeUrl = `${this.apiUrl}/v1/registries/nace`;
        this.apiSdgCodeUrl = `${this.apiUrl}/v1/registries/sdg`;
        this.apiRegistryUrl = `${this.apiUrl}/v1/registry/id`;
        this.apiEvidenceUrl = `${this.apiUrl}/v1/registries/evidence`;
        this.apiProvisionOrgGroupUrl = `${this.apiUrl}/v1/registries/evidence/taxonomy/provision_org_gro`;
        this.apiOrganizationsUrl = `${this.apiUrl}/v1/organizations`;
        this.apiUnitsUrl = `${this.apiUrl}/v1/units`;
        this.apiRegistriesUrl = `${this.apiUrl}/v1/registry/all`;
        this.apiEvidencesUrl = `${this.apiUrl}/v1/registries/evidence/all`;
        this.apiProvisionOrgGroupsUrl = `${this.apiUrl}/v1/registries/evidence/taxonomy/provision_org_gro`;
        this.maxRequestsPerSecond = (_b = opts === null || opts === void 0 ? void 0 : opts.maxRequestsPerSecond) !== null && _b !== void 0 ? _b : 10;
        this.defaultHeaders = {
            'Cache-Control': 'no-cache',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
        };
        if (this.apiKey) {
            this.defaultHeaders['x-api-key'] = `${this.apiKey}`;
        }
        this.reader = reader;
    }
    getPage({ url, params, }) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.get({
                url,
                params,
            }).then((r) => {
                if (!r.success) {
                    //TODO fix this
                    // throw new Error(r.message ?? 'Error')};
                    return {
                        data: [],
                        current_page: params === null || params === void 0 ? void 0 : params.page,
                        next_page: params === null || params === void 0 ? void 0 : params.page,
                        limit: params === null || params === void 0 ? void 0 : params.limit,
                        total: 0,
                    };
                }
                return r;
            });
        });
    }
}
exports.HttpEmddClient = HttpEmddClient;
