import { Api, EmddEvidence, EmddExtendedProcess, EmddNaceCode, EmddOrganization, EmddProcess, EmddProcessInfo, EmddProvisionOrgGroup, EmddRegistry, EmddSdgCode, EmddUnit, GetProcessesOptions, GetProcessOptions, Id, PaginationParams } from '.';
export type Headers = Record<string, string>;
export interface HttpGetParams {
    headers?: Headers;
    params?: Record<string, any>;
}
export interface HttpReader {
    get: <T>(url: string, config?: HttpGetParams) => Promise<T>;
}
export interface HttpEmddClientConfig {
    apiUrl?: string;
    apiKey?: string;
    maxRequestsPerSecond?: number;
}
export declare class HttpEmddClient implements Api {
    private readonly reader;
    private readonly apiUrl;
    private readonly apiKey?;
    private readonly apiProcessesSearchUrl;
    private readonly apiProcessesUrl;
    private readonly apiExtendedProcessesUrl;
    private readonly apiOrganizationUrl;
    private readonly apiNaceCodeUrl;
    private readonly apiSdgCodeUrl;
    private readonly apiUnitUrl;
    private readonly apiRegistryUrl;
    private readonly apiEvidenceUrl;
    private readonly apiProvisionOrgGroupUrl;
    private readonly apiOrganizationsUrl;
    private readonly apiUnitsUrl;
    private readonly apiRegistriesUrl;
    private readonly apiEvidencesUrl;
    private readonly apiProvisionOrgGroupsUrl;
    private readonly defaultHeaders;
    private readonly maxRequestsPerSecond;
    private activeRequestDates;
    constructor(reader: HttpReader, opts?: HttpEmddClientConfig);
    getSdgCode: (id: Id) => Promise<EmddSdgCode>;
    getNaceCode: (id: Id) => Promise<EmddNaceCode>;
    getUnit: (id: Id) => Promise<EmddUnit>;
    getOrganization: (id: Id) => Promise<EmddOrganization>;
    getProvisionOrgGroup: (id: Id) => Promise<EmddProvisionOrgGroup>;
    getRegistry: (id: Id) => Promise<EmddRegistry>;
    getEvidence: (id: Id) => Promise<EmddEvidence>;
    getProcess: (id: Id, opts?: GetProcessOptions) => Promise<EmddProcess>;
    getExtendedProcess: (id: Id, opts?: GetProcessOptions) => Promise<EmddExtendedProcess>;
    getProcesses: (opts?: GetProcessesOptions) => Promise<EmddProcessInfo[]>;
    getUnits: (params?: PaginationParams) => Promise<EmddUnit[]>;
    getOrganizations: (params?: PaginationParams) => Promise<EmddOrganization[]>;
    getRegistries: (params?: PaginationParams) => Promise<EmddRegistry[]>;
    getEvidences: () => Promise<EmddEvidence[]>;
    getProvisionOrgGroups: (params?: PaginationParams) => Promise<EmddProvisionOrgGroup[]>;
    private getPage;
    private exhaustAPI;
    private getOne;
    private get;
    private storeRequest;
}
