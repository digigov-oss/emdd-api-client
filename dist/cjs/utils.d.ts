export declare function executeOnCondition<T>(props: {
    f: () => Promise<T>;
    condition: () => boolean;
    beforeExecute?: () => void;
    delay: number;
}): Promise<T>;
