export function executeOnCondition(props) {
    const { f, beforeExecute, condition } = props;
    return new Promise((resolve, reject) => {
        function doExec() {
            beforeExecute && beforeExecute();
            f().then(resolve).catch(reject);
        }
        if (condition()) {
            doExec();
        }
        else {
            const interval = setInterval(() => {
                if (condition()) {
                    clearInterval(interval);
                    doExec();
                }
            }, props.delay);
        }
    });
}
