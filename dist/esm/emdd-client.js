import { executeOnCondition } from './utils';
export class HttpEmddClient {
    reader;
    apiUrl;
    apiKey;
    apiProcessesSearchUrl;
    apiProcessesUrl;
    apiExtendedProcessesUrl;
    apiOrganizationUrl;
    apiNaceCodeUrl;
    apiSdgCodeUrl;
    apiUnitUrl;
    apiRegistryUrl;
    apiEvidenceUrl;
    apiProvisionOrgGroupUrl;
    apiOrganizationsUrl;
    apiUnitsUrl;
    apiRegistriesUrl;
    apiEvidencesUrl;
    apiProvisionOrgGroupsUrl;
    defaultHeaders;
    maxRequestsPerSecond;
    activeRequestDates = new Array();
    constructor(reader, opts) {
        this.apiUrl = opts?.apiUrl ?? 'https://api.digigov.grnet.gr';
        this.apiKey = opts?.apiKey;
        this.apiProcessesSearchUrl = `${this.apiUrl}/v1/services/search`;
        this.apiProcessesUrl = `${this.apiUrl}/v1/services`;
        this.apiExtendedProcessesUrl = `${this.apiUrl}/v1/services-extended`;
        this.apiOrganizationUrl = `${this.apiUrl}/v1/organization`;
        this.apiUnitUrl = `${this.apiUrl}/v1/unit`;
        this.apiNaceCodeUrl = `${this.apiUrl}/v1/registries/nace`;
        this.apiSdgCodeUrl = `${this.apiUrl}/v1/registries/sdg`;
        this.apiRegistryUrl = `${this.apiUrl}/v1/registry/id`;
        this.apiEvidenceUrl = `${this.apiUrl}/v1/registries/evidence`;
        this.apiProvisionOrgGroupUrl = `${this.apiUrl}/v1/registries/evidence/taxonomy/provision_org_gro`;
        this.apiOrganizationsUrl = `${this.apiUrl}/v1/organizations`;
        this.apiUnitsUrl = `${this.apiUrl}/v1/units`;
        this.apiRegistriesUrl = `${this.apiUrl}/v1/registry/all`;
        this.apiEvidencesUrl = `${this.apiUrl}/v1/registries/evidence/all`;
        this.apiProvisionOrgGroupsUrl = `${this.apiUrl}/v1/registries/evidence/taxonomy/provision_org_gro`;
        this.maxRequestsPerSecond = opts?.maxRequestsPerSecond ?? 10;
        this.defaultHeaders = {
            'Cache-Control': 'no-cache',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
        };
        if (this.apiKey) {
            this.defaultHeaders['x-api-key'] = `${this.apiKey}`;
        }
        this.reader = reader;
    }
    //No plural available
    getSdgCode = async (id) => {
        return this.getOne({
            url: `${this.apiSdgCodeUrl}/${id}`,
        });
    };
    //No plural available
    getNaceCode = async (id) => {
        return this.getOne({
            url: `${this.apiNaceCodeUrl}/${id}`,
        });
    };
    getUnit = async (id) => {
        return this.getOne({ url: this.apiUnitUrl, params: { id } });
    };
    getOrganization = async (id) => {
        return this.getOne({
            url: this.apiOrganizationUrl,
            params: { id },
        });
    };
    getProvisionOrgGroup = async (id) => {
        return this.getOne({
            url: `${this.apiProvisionOrgGroupUrl}/${id}`,
        }).then((r) => {
            const res = {
                id: r.id,
                title: r.title,
                url: r.url,
            };
            return res;
        });
    };
    getRegistry = async (id) => {
        return this.getOne({
            url: `${this.apiRegistryUrl}/${id}`,
        });
    };
    getEvidence = async (id) => {
        return this.getOne({
            url: `${this.apiEvidenceUrl}/${id}`,
        });
    };
    getProcess = async (id, opts) => {
        const params = {};
        if (opts?.english === true) {
            params['lang'] = 'en';
        }
        return this.getOne({
            url: `${this.apiProcessesUrl}/${id}`,
            params,
        });
    };
    getExtendedProcess = async (id, opts) => {
        const params = {};
        if (opts?.english === true) {
            params['lang'] = 'en';
        }
        return this.getOne({
            url: `${this.apiExtendedProcessesUrl}/${id}`,
            params,
        });
    };
    getProcesses = async (opts) => {
        let url = `${this.apiProcessesUrl}`;
        if (opts?.filter) {
            url = `${this.apiProcessesSearchUrl}/filter/${opts.filter}`;
        }
        const params = {};
        if (opts?.english === true) {
            params.lang = 'en';
        }
        if (opts?.limit || opts?.page) {
            params.page = opts.page;
            params.limit = opts.limit;
            return this.getPage({
                url,
                params: params,
            }).then((r) => r.data);
        }
        return this.exhaustAPI({ url, params });
    };
    getUnits = async (params) => {
        const url = this.apiUnitsUrl;
        if (params) {
            return this.getPage({ url, params }).then((r) => r.data);
        }
        return this.exhaustAPI({ url });
    };
    getOrganizations = async (params) => {
        const url = this.apiOrganizationsUrl;
        if (params) {
            return this.getPage({ url, params }).then((r) => r.data);
        }
        return this.exhaustAPI({ url });
    };
    getRegistries = async (params) => {
        const url = this.apiRegistriesUrl;
        if (params) {
            return this.getPage({ url, params }).then((r) => r.data);
        }
        return this.exhaustAPI({ url });
    };
    getEvidences = async () => {
        const url = this.apiEvidencesUrl;
        return this.get({ url }).then((r) => r.data);
    };
    getProvisionOrgGroups = async (params) => {
        const url = this.apiProvisionOrgGroupsUrl;
        if (params) {
            return this.getPage({ url, params }).then((r) => r.data);
        }
        return this.exhaustAPI({ url });
    };
    async getPage({ url, params, }) {
        return this.get({
            url,
            params,
        }).then((r) => {
            if (!r.success) {
                //TODO fix this
                // throw new Error(r.message ?? 'Error')};
                return {
                    data: [],
                    current_page: params?.page,
                    next_page: params?.page,
                    limit: params?.limit,
                    total: 0,
                };
            }
            return r;
        });
    }
    exhaustAPI = ({ url, params, }) => {
        const limit = 100;
        return new Promise((resolve, reject) => {
            const allPages = [];
            const page = 1;
            return this.getPage({
                url,
                params: Object.assign({}, params, { page, limit }),
            })
                .then((r) => {
                const total = r.total;
                let pages = Math.floor(total / limit);
                if (total % limit !== 0) {
                    pages = pages + 1;
                }
                const promises = Array();
                allPages[1] = r.data;
                for (let page = 2; page <= pages; page++) {
                    promises.push(this.getPage({
                        url,
                        params: Object.assign({}, params, {
                            page,
                            limit,
                        }),
                    }).then((r) => {
                        allPages[page] = r.data;
                    }));
                }
                Promise.all(promises)
                    .then(() => {
                    let res = Array();
                    const dataArray = Object.entries(allPages)
                        .sort((a, b) => {
                        if (a[0] === b[0])
                            return 0;
                        return a[0] < b[0] ? -1 : 1;
                    })
                        .map((a) => {
                        return a[1];
                    });
                    res = res.concat(...dataArray);
                    resolve(res);
                })
                    .catch(reject);
            })
                .catch(reject);
        });
    };
    getOne = async (opts) => {
        return this.get(opts).then((r) => {
            if (!r.success)
                throw new Error(r.message ?? 'Error');
            return r.data;
        });
    };
    get = async (opts) => {
        const url = opts.url;
        return executeOnCondition({
            f: async () => {
                return this.reader.get(url, {
                    params: opts?.params,
                    headers: this.defaultHeaders,
                });
            },
            condition: () => {
                if (this.activeRequestDates.length < this.maxRequestsPerSecond) {
                    return true;
                }
                return new Date().getTime() - 1000 > this.activeRequestDates[0];
            },
            beforeExecute: () => {
                this.storeRequest();
            },
            delay: 50,
        });
    };
    storeRequest = () => {
        this.activeRequestDates.push(new Date().getTime());
        const l = this.activeRequestDates.length;
        if (l > this.maxRequestsPerSecond) {
            this.activeRequestDates.shift();
        }
    };
}
