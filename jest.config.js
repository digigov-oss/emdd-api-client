export default {
    // The test environment that will be used for testing
    testEnvironment: 'node',
    testTimeout: 60 * 1000, // 1 minute
    // The glob patterns Jest uses to detect test files
    testMatch: ['**/test/**/*.spec.ts'],

    // The file extensions Jest will look for
    moduleFileExtensions: ['ts', 'js'],

    // The paths to modules that run some code to configure or set up the testing environment
    //setupFilesAfterEnv: ['./jest.setup.ts'],

    // The paths to modules that Jest should use to transform your code before running tests
    transform: {
      '^.+\\.ts$': 'ts-jest',
    },

    // The directories that Jest should search for files in
    roots: ['<rootDir>/test/src'],

    // The coverage report format
    coverageReporters: ['text', 'lcov'],

    // The coverage threshold values
    coverageThreshold: {
      global: {
        branches: 80,
        functions: 80,
        lines: 80,
        statements: 80,
      },
    },
    setupFiles: ['<rootDir>/test/setupTests.ts']
  };