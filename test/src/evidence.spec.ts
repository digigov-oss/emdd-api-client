import inspect from 'object-inspect';
import { client } from './client';

const log = false;

describe('evidence api getters', () => {
    it('should return an evidence code by id', async () => {
        await client.getEvidence('2825').then((response) => {
            if (log) {
                console.log(inspect(response, { depth: 10, indent: 2 }));
            }
        });
    });

    it('should be able to hit evidence endpoint timely', async () => {
        const timeId = 'get evidence many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getEvidence('2825'));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });

    it(
        'should return all evidences',
        async () => {
            const timeId = 'get evidences';
            console.time(timeId);
            await client.getEvidences();
            console.timeEnd(timeId);
        },
        2.5 * 60 * 1000,
    );
});
