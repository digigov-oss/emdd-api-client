import inspect from 'object-inspect';
import { client } from './client';

const log = false;

describe('unit api getters', () => {
    it('should return an unit code by id', async () => {
        await client.getUnit(528385).then((response) => {
            if (log) {
                console.log(inspect(response, { depth: 10, indent: 2 }));
            }
        });
    });

    it('should be able to hit unit endpoint timely', async () => {
        const timeId = 'get unit many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getUnit(528385));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });

    it(
        'should return all units',
        async () => {
            const timeId = 'get units';
            console.time(timeId);
            await client.getUnits();
            console.timeEnd(timeId);
        },
        10 * 60 * 1000,
    );
});
