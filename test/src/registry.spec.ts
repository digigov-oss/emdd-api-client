import inspect from 'object-inspect';
import { client } from './client';

const log = false;

describe('registry api getters', () => {
    it('should return an registry code by id', async () => {
        await client.getRegistry('293446').then((response) => {
            if (log) {
                console.log(inspect(response, { depth: 10, indent: 2 }));
            }
        });
    });

    it('should be able to hit registry endpoint timely', async () => {
        const timeId = 'get registry many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getRegistry('293446'));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });

    it(
        'should return all registries',
        async () => {
            const timeId = 'get registries';
            console.time(timeId);
            await client.getRegistries();
            console.timeEnd(timeId);
        },
        2.5 * 60 * 1000,
    );
});
