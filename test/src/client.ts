import axios, { AxiosError } from 'axios';
import axiosRetry from 'axios-retry';

import {
    HttpEmddClient,
    HttpGetParams,
    HttpReader,
} from '../../src/emdd-client';

axiosRetry(axios, {
    retries: 5,
    retryDelay: (...arg) => axiosRetry.exponentialDelay(...arg, 300),
    retryCondition(error: AxiosError) {
        if (error.response)
            switch (error.response.status) {
                //retry only if status is 500 or 501 or 503
                case 500:
                case 501:
                case 503:
                    return true;
                default:
                    return false;
            }
        return false;
    },
    onRetry: (retryCount, error) => {
        console.log(`retry count: ${retryCount} ${error.response?.config.url}`);
        console.log(error.response?.config.params);
    },
});

let i = 0;
const d = new Date();
const logCounts = false;
const axiosReader: HttpReader = {
    get: async <T>(url: string, config?: HttpGetParams) => {
        return axios
            .get<T>(url, config)
            .then((r) => {
                return r.data;
            })
            .then((r) => {
                if (logCounts) {
                    i++;
                    console.log(
                        `requests = ${i} ${
                            (new Date().getTime() - d.getTime()) / 1000
                        }`,
                    );
                }
                return r;
            })
            .catch((e: AxiosError) => {
                console.log(e);
                if (e.response?.status === 404) {
                    const resp = e.response;
                    console.log(`${resp.config.url}`);
                }
                throw e;
            });
    },
};

export const client = new HttpEmddClient(axiosReader, {
    apiUrl: 'https://api.digigov.grnet.gr',
    maxRequestsPerSecond: 10,
});
