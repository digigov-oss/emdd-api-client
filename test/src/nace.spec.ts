import inspect from 'object-inspect';
import { client } from './client';

const log = false;
describe('nace api getters', () => {
    it('should return an nace code by id', async () => {
        await client.getNaceCode('38.12').then((response) => {
            if (log) {
                console.log(inspect(response, { depth: 10, indent: 2 }));
            }
        });
    });

    it.only('should be able to hit nace endpoint timely', async () => {
        const timeId = 'get nace many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getNaceCode('38.12'));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });
});
