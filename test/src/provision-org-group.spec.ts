import inspect from 'object-inspect';
import { client } from './client';

const log = false;

describe('provision org group api getters', () => {
    it('should return an provision org group code by id', async () => {
        await client.getProvisionOrgGroup('3621').then((response) => {
            if (log) {
                console.log(inspect(response, { depth: 10, indent: 2 }));
            }
        });
    });

    it('should be able to hit provision org group endpoint timely', async () => {
        const timeId = 'get provision org group many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getProvisionOrgGroup('3621'));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });

    it(
        'should return all provision org groups',
        async () => {
            const timeId = 'get provision org groups';
            console.time(timeId);
            await client.getProvisionOrgGroups().then(async (response) => {
                if (log) {
                    console.log(inspect(response, { depth: 10, indent: 2 }));
                }
            });
            console.timeEnd(timeId);
        },
        2.5 * 60 * 1000,
    );
});
