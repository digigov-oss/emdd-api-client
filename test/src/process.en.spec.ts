import inspect from 'object-inspect';
import { client } from './client';

const log = false;

describe('process english api getters', () => {
    it('should return an english process by id', async () => {
        await client
            .getProcess('822764', { english: true })
            .then((response) => {
                if (log) {
                    console.log(inspect(response, { depth: 10, indent: 2 }));
                }
            });
    });

    it('should be able to hit english process endpoint timely', async () => {
        const timeId = 'get english process many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getProcess('822764', { english: true }));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });

    it(
        'should return english processes with params',
        async () => {
            const timeId = 'get english processes';
            console.time(timeId);
            await client.getProcesses({ limit: 50, english: true });
            console.timeEnd(timeId);
        },
        2.5 * 60 * 1000,
    );

    it(
        'should return all english processes without params',
        async () => {
            const timeId = 'get english processes';
            console.time(timeId);
            await client.getProcesses({ english: true });
            console.timeEnd(timeId);
        },
        2.5 * 60 * 1000,
    );
});
