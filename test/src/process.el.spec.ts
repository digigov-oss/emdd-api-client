import inspect from 'object-inspect';
import { client } from './client';

const log = false;

describe('process api getters', () => {
    it('should return a process by id', async () => {
        await client.getProcess('100201').then((response) => {
            if (log) {
                console.log(inspect(response, { depth: 10, indent: 2 }));
            }
        });
    });

    it('should be able to hit process endpoint timely', async () => {
        const timeId = 'get process many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getProcess('100201'));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });

    it(
        'should return processes with params',
        async () => {
            const timeId = 'get processes';
            console.time(timeId);
            await client.getProcesses({ limit: 50 });
            console.timeEnd(timeId);
        },
        2.5 * 60 * 1000,
    );

    it(
        'should return all processes',
        async () => {
            const timeId = 'get processes';
            console.time(timeId);
            await client.getProcesses();
            console.timeEnd(timeId);
        },
        2.5 * 60 * 1000,
    );
});
