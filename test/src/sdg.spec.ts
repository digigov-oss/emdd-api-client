import inspect from 'object-inspect';
import { client } from './client';

const log = false;

describe('sdg api getters', () => {
    it('should return an sdg code by id', async () => {
        await client.getSdgCode('N1').then((response) => {
            if (log) {
                console.log(inspect(response, { depth: 10, indent: 2 }));
            }
        });
    });

    it('should be able to hit sdg endpoint timely', async () => {
        const timeId = 'get sdg many times';
        console.time(timeId);
        const promises: Array<Promise<any>> = [];
        for (let i = 0; i < 50; i++) {
            promises.push(client.getSdgCode('N1'));
        }
        await Promise.all(promises);
        console.timeEnd(timeId);
    });
});
